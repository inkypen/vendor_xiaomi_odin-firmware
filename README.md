# vendor_xiaomi_odin-firmware

Firmware images for MIX 4 (odin), to include in custom ROM builds.

**Current version**: fw_odin_miui_ODIN_V14.0.6.0.TKMCNXM_444a9dea66_13.0

### How to use?

1. Clone this repo to `vendor/xiaomi/odin-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/odin-firmware/BoardConfigVendor.mk
```

